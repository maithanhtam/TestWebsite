package wit.screens;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import wit.core.Driver;

import java.util.List;

/**
 * Created by thuynguyen on 8/25/18.
 */
public class Datatable {


    private static String TITLE_GLOBAL_WS = "DataTables | Table plug-in for jQuery";


    public static void openDatatablePage() throws InterruptedException {
        Driver.setDriver();
        Driver.getDriver().get("https://datatables.net/");
        Driver.getDriver().manage().window().maximize();
        Thread.sleep(2000);
    }

    public static void isDataTablePage(String expectedTitle) {

        //Getting the actual Title
        String actualTitle = "";
        actualTitle = Driver.getDriver().getTitle();//selenium WebDriver.//cau lenh lay title cua trang web do
        //Validating the test case
        Assert.assertEquals(expectedTitle, actualTitle);

    }

    public static void inputValuesInsideForm1() throws InterruptedException {

        WebElement tbody = Driver.getDriver().findElement(By.id("example_wrapper")).findElement(By.tagName("tbody"));

        List<WebElement> listTr = tbody.findElements(By.tagName("tr"));

        for (WebElement elementTr : listTr) {
            List<WebElement> listTd = elementTr.findElements(By.tagName("td"));
            //tim tat ca gia tri cac dong cua cot 3: all tr, tung td3
            //   for (WebElement elementTd:listTd) {
            System.out.println("gia tri cot 3 cua dong la " + listTd.get(2).getText());
            if (listTd.get(2).getText().equals("London")) {
                System.out.println("Name "+listTd.get(0).getText() +" - "+"Possition "+listTd.get(1).getText());



            }
        }


    }
    public static void inputValuesInsideForm2() throws InterruptedException {

        WebElement tbody = Driver.getDriver().findElement(By.id("example_wrapper")).findElement(By.tagName("tbody"));
        List<WebElement> listPaging = Driver.getDriver().findElement(By.id("example_paginate")).findElement(By.tagName("span")).findElements(By.tagName("a"));

        for (WebElement page:listPaging){
            page.click();

            List<WebElement> listTr = tbody.findElements(By.tagName("tr"));

            for (WebElement elementTr : listTr) {
                List<WebElement> listTd = elementTr.findElements(By.tagName("td"));
                //tim tat ca gia tri cac dong cua cot 3: all tr, tung td3
                //   for (WebElement elementTd:listTd) {
                System.out.println("gia tri cot 3 cua dong la " + listTd.get(2).getText());
                if (listTd.get(2).getText().equals("London")) {
                    System.out.println("Name "+listTd.get(0).getText() +" - "+"Possition "+listTd.get(1).getText());



                }
            }

        }




    }

    public static void main(String[] args) throws InterruptedException {
        openDatatablePage();
        isDataTablePage(TITLE_GLOBAL_WS);
        inputValuesInsideForm1();
    }


}
